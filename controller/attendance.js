const diff = require('arr-diff')
const dateFormat = require('dateformat')
const modelAttendance = require('../model/attendance')

async function studentGetHandler(ctx,next){ 
    let currentDate = dateFormat(new Date(),'yyyy-mm-dd')
    const txtDate = dateFormat(currentDate,'dd-mmm-yyyy')
    let club = "All"
    const stuedentsOfClub = await modelAttendance.getStuden(club,currentDate)
    const clubName = await modelAttendance.getClubName()

    await ctx.render('attendance',{
        clubname: clubName,
        students: stuedentsOfClub,
        dllClubname: club,
        txtDate: txtDate,
        altSuccess: '',
        club:club
    });
}

async function studentPostHandler(ctx,next){
    let club = ctx.request.body.club;
    let date = ctx.request.body.calendar;
    let term = ctx.request.body.term;
    let chkStudent = ctx.request.body.chkStudent;
    let btnAction = ctx.request.body.btnAction;
    console.log(ctx.request.body)

    let dateTime,txtDate
    let clubName
    let students
    dateTime = dateFormat(date,'yyyy-mm-dd')
    txtDate = dateFormat(date,'dd-mmm-yyyy')
    students = await modelAttendance.getStuden(club,dateTime)

    clubName = await modelAttendance.getClubName()
    await ctx.render('attendance',{
        clubname: clubName,
        students: students,
        dllClubname: club,
        txtDate: txtDate,
        altSuccess: '',
        club:club
    });  

    console.log(students);
    if(btnAction == 'btnSubmit'){
        if(students.length){
            await attendanceStudent(club,dateTime,chkStudent)

            // render after update 
            dateTime = dateFormat(date,'yyyy-mm-dd')
            txtDate = dateFormat(date,'dd-mmm-yyyy')
            students = await modelAttendance.getStuden(club,dateTime)
            clubName = await modelAttendance.getClubName()
            await ctx.render('attendance',{
                clubname: clubName,
                students: students,
                dllClubname: club,
                txtDate: txtDate,
                altSuccess: 'Update Successfully!',
                club:club,
            }); 
        }
    }
}

async function attendanceStudent(club,dateTime,chkStudent){
    let sidCheck = null
    let sidUncheck = null

    const studentCheck = await modelAttendance.getStuden(club,dateTime)
    const arrStudent = studentCheck.map(student => { 
        return student.sid 
    })
    
    if(chkStudent != undefined){ 
        let statusCheck = null
        if(chkStudent.length == 1){
            let checkOne = []
            checkOne.push(parseInt(chkStudent))
            statusCheck = checkOne
        }else{
            statusCheck = chkStudent.map(status => {
                return parseInt(status)
            })
        }

        const statusUncheck = diff(arrStudent, statusCheck)
        sidCheck = statusCheck.toString()
        sidUncheck = statusUncheck.toString()
    }

    if(chkStudent == undefined){
        sidUncheck = arrStudent.toString()
    }
    
    if(sidUncheck){
        modelAttendance.attendanceAbsent(sidUncheck);
    }
    if(sidCheck){
        modelAttendance.attendancePresent(sidCheck);
    }
}

module.exports = {
    studentGetHandler,
    studentPostHandler
}