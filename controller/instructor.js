const modelInstructor = require('../model/instructor')

async function instructorGetHandler(ctx,next){
    const instructor = await modelInstructor.getInstructor()
    await ctx.render('instructor',{
        instructors: instructor
    })
}

module.exports = {
    instructorGetHandler
}


