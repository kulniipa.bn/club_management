'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const koaBody = require('koa-body')

const router = new Router()
const app = new Koa()

const controllerAttendance = require('./controller/attendance')
const controllerInstructor = require('./controller/instructor')

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
});
  
router.get('/instructor',controllerInstructor.instructorGetHandler)
router.get('/attendance',controllerAttendance.studentGetHandler)
router.post('/attendance',controllerAttendance.studentPostHandler)



app.use(koaBody())
app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(5000)
