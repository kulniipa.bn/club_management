const instructor = require('./instructor')
const attendance = require('./attendance')

module.exports ={
    instructor,
    attendance
}