const pool = require('../db/index')

async function getClubName(){
    const sql = `SELECT cid,clubname,day FROM tbl_clubs`
    const [result] = await pool.query(sql)
    return result
}

async function getStuden(club,date){
    let strClub
    (club == "All") ? strClub = `C.clubname LIKE '%%'` : strClub = `C.clubname='${club}'`
    const [result] = await pool.query(`SELECT S.sid,S.firstname,S.lastname,S.year,S.delete_status,S.update_date,IF(S.delete_status=1,"checked","") As delete_status,C.clubname 
                                        FROM tbl_students As S 
                                        LEFT JOIN tbl_club_reltionship As R ON S.sid=R.sid 
                                        LEFT JOIN tbl_clubs AS C ON C.cid=R.cid
                                        WHERE ${strClub} AND S.update_date LIKE '%${date}%'`)
    return result                    
}

async function attendanceAbsent(sidUncheck){
    await pool.query(`UPDATE tbl_students SET delete_status = 0 WHERE sid IN (${sidUncheck})`)
}

async function attendancePresent(sidCheck){
    await pool.query(`UPDATE tbl_students SET delete_status = 1 WHERE sid IN (${sidCheck})`)
}

module.exports = {
    getClubName,
    getStuden,
    attendanceAbsent,
    attendancePresent
}