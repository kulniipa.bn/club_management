const pool = require('../db/index')

async function getInstructor(){
    const [result] = await pool.query(`SELECT tid As ID, firstname As Firstname, 
                                        lastname As Lastname, Tel As Phone, 
                                        email As Email, create_by As 'Create By', 
                                        DATE_FORMAT(create_date, '%d-%b-%Y %H:%i') As Date, 
                                        update_by As 'Update By', update_date As 'Update Date', 
                                        IF(delete_status=1,'Absent','Present') As 'Status',
                                        '' As 'Edit/Delete'
                                    FROM 
                                        tbl_teachers`)
                                        
    return result
}

module.exports = {
    getInstructor
}